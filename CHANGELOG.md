# Changelog
All notable changes to this project will be documented in this file.

## [1.0.2] - 2019-10-16
### Changed
- No quotation marks required for string values in DATA statement (comma separated strings).

### Removed
- WHILE and WEND keywords.

## [1.0.1] - 2019-09-23
### Added
- Changelog file.

### Changed
- Does not tokenize the rest of the line after REM keyword or comment character.
- Show line number in 'Unterminated string' error message.

## [1.0.0] - 2019-09-12
### Added
- Initial release.